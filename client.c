#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <math.h>
//client:october
//server:fall
//Note: PLease run server code at Host fall before runing client code on october.


char substitution( int encryption, int method_number, char orignal_char ) // encryption=1 if we want encrypt, 0 if we want decrypt. eg if we using S1, method_number=1, and orignal_char is the char we want to encrypt or decryption
{
  char re_val;
  // as required in the question, I set N=5, r=5, n=5,m=40,
  char before_substitution[40]={'a','b','c','d','e',
                                'f','g','h','i','j',
                                'k','l','m','n','o',
                                'p','q','r','s','t',
                                'u','v','w','x','y',
                                'z','0','1','2','3',
                                '4','5','6','7','8',
                                '9',' ',',','.','?'};
  // define substitution of S1
  char after_substitution_S0[40]={'p','l','m','o','k',
                                  'n','i','j','b','u',
                                  'h','v','0','1','2',
                                  '3','4','5','6','7',
                                  '8','9','y','g','c',
                                  'q',' ',',','.','?',
                                  't','f','x','r','d',
                                  'z','e','s','w','a'};
// define substitution of S2
 char after_substitution_S1[40]={'q','a','s','d','f',
                                'g','h','k','j','l',
                                'z','x','c','v','b',
                                '0','1','2','3','4',
                                'n','m','w','e','r',
                                '9','8','7','6','5',
                                'p',',',' ','.','?',
                                't','y','u','i','o'};
// define substitution of S3
 char after_substitution_S2[40]={
                                'z','e','s','w','a',
                                't','f','x','r','d',
                                'q',' ',',','.','?',
                                '8','9','y','g','c',
                                'p','l','m','o','k',
                                'h','v','0','1','2',
                                'n','i','j','b','u',
                                '3','4','5','6','7'};
// define substitution of S4
 char after_substitution_S3[40]={
                                '8','9','y','g','c',
                                'n','i','j','b','u',
                                'p','l','m','o','k',
                                'h','v','0','1','2',
                                't','f','x','r','d',
                                'q',' ',',','.','?',
                                'z','e','s','w','a',
                                '5','7','6','4','3'};
// define substitution of S5
 char after_substitution_S4[40]={'9','6','3','8','5',
                                '2','7','4','1','0',
                                'q','p','w','o','e',
                                'i','r','u','t','y',
                                'a','l','s','k','d',
                                'j','f','h','g','z',
                                'm',',',' ','.','?',
                                'x','n','c','b','v'};


  int i=0;
  if (encryption==1) //doing encryption
  {
    if (method_number==0)//S0
    {
      for(i=0;i<40;i++)
      {
        if (orignal_char==before_substitution[i]) return after_substitution_S0[i];

      }
    }
    else if (method_number==1)//S1
    {
      for(i=0;i<40;i++)
      {
        if (orignal_char==before_substitution[i]) return after_substitution_S1[i];

      }

    }
    else if(method_number==2)//S2
    {
      for(i=0;i<40;i++)
      {
        if (orignal_char==before_substitution[i]) return after_substitution_S2[i];

      }
    }
    else if(method_number==3)//S3
    {
      for(i=0;i<40;i++)
      {
        if (orignal_char==before_substitution[i]) return after_substitution_S3[i];

      }
    }
    else if(method_number==4)//S4
    {
      for(i=0;i<40;i++)
      {
        if (orignal_char==before_substitution[i]) return after_substitution_S4[i];

      }
    }
  }
  else if (encryption==0)//doing decryption
  {
    if (method_number==0)//S0
    {
      for(i=0;i<40;i++)
      {
        if (orignal_char==after_substitution_S0[i]) return before_substitution[i];

      }
    }
    else if (method_number==1)//S1
    {
      for(i=0;i<40;i++)
      {
        if (orignal_char==after_substitution_S1[i]) return before_substitution[i];

      }

    }
    else if(method_number==2)//S2
    {
      for(i=0;i<40;i++)
      {
      if (orignal_char==after_substitution_S2[i]) return before_substitution[i];

      }
    }
    else if(method_number==3)//S3
    {
      for(i=0;i<40;i++)
      {
        if (orignal_char==after_substitution_S3[i]) return before_substitution[i];

      }
    }
    else if(method_number==4)//S4
    {
      for(i=0;i<40;i++)
      {
        if (orignal_char==after_substitution_S4[i]) return before_substitution[i];

      }
    }
  }
  return '{';//indicate invaild argument values
}

int main()
{
    // as required in the question, I set N=5, r=5, n=5,m=40, Si uses ith security scheme

  //Creating socket
  printf("------------------------------------output of the program------------------------\n");
  printf("creating socket\n");
  int newsocket = socket(PF_INET , SOCK_STREAM , 0); // TCP/IP family
  if (newsocket==-1){
    printf("creation failed\n");
    return;

  }
  else{
  printf("socket has been created\n");
  }
  struct sockaddr_in serveraddress; //specify endpoints address
  serveraddress.sin_family=AF_INET; //IP
  serveraddress.sin_port=htons(7000);// convert host byte order to network byte order
  serveraddress.sin_addr.s_addr=inet_addr("172.16.1.14"); //conver dot notation of IP address into internet host address
  int return_of_connect=connect(newsocket, (struct sockaddr*) &serveraddress, sizeof(serveraddress));
  if(return_of_connect<0){
    printf("connction fails, unable to conect\n");
    return;
  }
  else{
    printf("conncetion complete\n");
  }
  // conncetion complete
  // printf("Please shut down eth1 using ifdown eth1\n");
  // start to sending
  // printf("please enter the message you want to send, maxium size is 500 characters\n");
  //getting the integer k
  printf("getting integer value k.......................\n");
  int n=10; //public number
  int q=12;//public number

  int x_a=5;// private number
  int n_to_power_of_x_a=1;
  int count=0;
  while (count<x_a)
  {
    n_to_power_of_x_a=n_to_power_of_x_a*n;
    count++;
  }
  int y_a[1]={n_to_power_of_x_a%q};
  // send the value of y_a to server
  int return_of_send=send(newsocket, y_a, sizeof(int),0);
  printf("y_a I send is %d", y_a[0]);
  if(return_of_send<0){
    printf("unable to send y_a, send fails\n");
    return;
  }
  printf("trying to recieve the value of y_b from server\n");
  int re_y_b[1];
  int return_of_recieve=recv(newsocket,re_y_b,sizeof(int),0);
  printf("y_b I recieved is %d", re_y_b[0]);
  if(return_of_recieve<0){
    printf("recieve of y_b fails, cannot do the rest\n");
    return;

  }
  else{
    printf("y_b recieved, calculating the k value\n");
  }
  int y_b=re_y_b[0];
  // calculate k value
  int k;
  int y_b_to_the_power_of_x_a=1;
  count=0;
  while(count<x_a)
  {
    y_b_to_the_power_of_x_a=y_b_to_the_power_of_x_a * y_b;
    count++;
  }
  k=y_b_to_the_power_of_x_a % q; //getting k value;
  //N=5 in our case
  printf("the value of integer k is %d\n",k);
  printf("N=5 in this program, we have 5 security schemes, they are S0 S1 S2 S3 S4, calculating which one to use ......" );
  int method=k % 5;
  printf("we will use S%d\n",method);
  char sending_Str[500]="hello, this is a secure message";
  printf("before encrprtion, the message need to be sent is :\n");
  printf("%s\n",sending_Str);
  // doing encrprtion
  int i=0;
  int j=0;
  int o=0;
  for (i=0;i<strlen(sending_Str);i++)
  {
    sending_Str[i]=substitution(1,method,sending_Str[i]);//substitution first
  }
  //do a transpostion
  int tem=25; //r=5,n=5.
  int number_of_matrix;//calculat how many 5*5 matrix we need
  if(strlen(sending_Str)%tem==0)
  {
    number_of_matrix=strlen(sending_Str)/tem;
  }
  else
  {
    number_of_matrix=strlen(sending_Str)/tem+1;
  }
  char matrix[number_of_matrix][5][5]; // untransposed matrices. matrix[0][x][y] is the frist matrix matrix[1][x][y] is the second matrix and so on.
  //put the substituted message into matrices
  for(i=0;i<number_of_matrix;i++)
  {
    for(j=0;j<5;j++)// indicate which row
    {
      for(o=0;o<5;o++) // indicate which coloum
      {
        if(o+5*j+5*5*i<strlen(sending_Str))
        {
          matrix[i][j][o]=sending_Str[o+5*j+5*5*i];
        }
        else
        {
         matrix[i][j][o]='-';
        }
      }

    }
  }
  // doing transpostion on these matrices
  char matrix2[number_of_matrix][5][5]; // matrices that will store the transposed matrices
  for(i=0;i<number_of_matrix;i++)
  {
    for(j=0;j<5;j++)// indicate which row
    {
      for(o=0;o<5;o++)// indicate which coloum
      {
        matrix2[i][j][o]=matrix[i][o][j];
      }

    }
  }// done transpostion
  char actual_sending_Str[500];
  int index=0;
  for(i=0;i<number_of_matrix;i++)
  {
    for(j=0;j<5;j++)// indicate which row
    {
      for(o=0;o<5;o++)// indicate which coloum
      {
        actual_sending_Str[index]=matrix2[i][j][o];
        index++;
      }

    }
  }
  actual_sending_Str[index]='\0';
  int len=strlen(actual_sending_Str);
  return_of_send=send(newsocket, actual_sending_Str, len,0);// send the encrypted message
  if(return_of_send<0){
    printf("unable to send, send fails\n");
    return;
  }
  else{
    printf("send complete, the encrypted message is :\n" );
    printf("%s\n",actual_sending_Str);
  }



  //recieve encrypted message from server
  printf("trying to recieve from server, maxium size 500 characters\n");
  char reply_Str[500];
  return_of_recieve=recv(newsocket,reply_Str,500,0);
  if(return_of_recieve<0){
    printf("recieve fails, reply not recieved\n");
    return;

  }
  else{
    printf("reply recieved, the reply message is (without decryption):\n");
    printf("%s\n",reply_Str);
  }



  //doing decryption

  //start to do  a transpostion


  tem=25; //r=5,n=5.
  //calculat how many 5*5 matrix we need to store this message
  if(strlen(reply_Str)%tem==0)
  {
    number_of_matrix=strlen(reply_Str)/tem;
  }
  else
  {
    number_of_matrix=strlen(reply_Str)/tem+1;
  }
  char matrix3[number_of_matrix][5][5];
  //put the recieved message into matrices
  for(i=0;i<number_of_matrix;i++)
  {
    for(j=0;j<5;j++)// indicate which row
    {
      for(o=0;o<5;o++) // indicate which coloum
      {
          matrix3[i][j][o]=reply_Str[o+5*j+5*5*i];

      }

    }
  }






// doing transpostion on these matrices
  char matrix4[number_of_matrix][5][5]; // matrices that will store the transposed matrices
  for(i=0;i<number_of_matrix;i++)
  {
    for(j=0;j<5;j++)// indicate which row
    {
      for(o=0;o<5;o++)// indicate which coloum
      {
        matrix4[i][j][o]=matrix3[i][o][j];
      }

    }
  }// done transpostion
  // put transposed matrix into a string
  char actual_reply_Str[500];
  index=0;
  for(i=0;i<number_of_matrix;i++)
  {
    for(j=0;j<5;j++)// indicate which row
    {
      for(o=0;o<5;o++)// indicate which coloum
      {
        actual_reply_Str[index]=matrix4[i][j][o];
        index++;
      }

    }
  }
  actual_reply_Str[index]='\0';

  // now we have to do a substitution to get orignal message. So we need to call substution function again
  //printf("transposition  completed, doing substitution................\n");
  char  actual_reply_Str2[500];
  for (i=0;i<strlen(actual_reply_Str);i++)
  {
    if(actual_reply_Str[i]=='-') //'-' should only happen at the end
    {
      //actual_reply_Str2[i]='\0';
      break;
    }
    actual_reply_Str2[i]=substitution(0,method,actual_reply_Str[i]);//first argument=0 since we doing decrpytion
  }
  actual_reply_Str2[i]='\0';
  printf("succesful decrpytion of reply message \n" );
  printf("the actual reply message is :\n");
  printf("%s\n",actual_reply_Str2);

  // shutdown socket and close it
  shutdown(newsocket,2);
  close(newsocket);
  printf("------------------------------------end of output------------------------\n");
  return 0;

}
