#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <math.h>
//client:october
//server:fall
//Note: PLease run server code at Host fall before runing client code on october.
char substitution( int encryption, int method_number, char orignal_char ) // encryption=1 if we want encrypt, 0 if we want decrypt. eg if we using S1, method_number=1, and orignal_char is the char we want to encrypt or decryption
{
  char re_val;
  // as required in the question, I set N=5, r=5, n=5,m=40,
  char before_substitution[40]={'a','b','c','d','e',
                                'f','g','h','i','j',
                                'k','l','m','n','o',
                                'p','q','r','s','t',
                                'u','v','w','x','y',
                                'z','0','1','2','3',
                                '4','5','6','7','8',
                                '9',' ',',','.','?'};
  // define substitution of S1
  char after_substitution_S0[40]={'p','l','m','o','k',
                                  'n','i','j','b','u',
                                  'h','v','0','1','2',
                                  '3','4','5','6','7',
                                  '8','9','y','g','c',
                                  'q',' ',',','.','?',
                                  't','f','x','r','d',
                                  'z','e','s','w','a'};
// define substitution of S2
 char after_substitution_S1[40]={'q','a','s','d','f',
                                'g','h','k','j','l',
                                'z','x','c','v','b',
                                '0','1','2','3','4',
                                'n','m','w','e','r',
                                '9','8','7','6','5',
                                'p',',',' ','.','?',
                                't','y','u','i','o'};
// define substitution of S3
 char after_substitution_S2[40]={
                                'z','e','s','w','a',
                                't','f','x','r','d',
                                'q',' ',',','.','?',
                                '8','9','y','g','c',
                                'p','l','m','o','k',
                                'h','v','0','1','2',
                                'n','i','j','b','u',
                                '3','4','5','6','7'};
// define substitution of S4
 char after_substitution_S3[40]={
                                '8','9','y','g','c',
                                'n','i','j','b','u',
                                'p','l','m','o','k',
                                'h','v','0','1','2',
                                't','f','x','r','d',
                                'q',' ',',','.','?',
                                'z','e','s','w','a',
                                '5','7','6','4','3'};
// define substitution of S5
 char after_substitution_S4[40]={'9','6','3','8','5',
                                '2','7','4','1','0',
                                'q','p','w','o','e',
                                'i','r','u','t','y',
                                'a','l','s','k','d',
                                'j','f','h','g','z',
                                'm',',',' ','.','?',
                                'x','n','c','b','v'};


  int i=0;
  if (encryption==1) //doing encryption
  {
    if (method_number==0)//S0
    {
      for(i=0;i<40;i++)
      {
        if (orignal_char==before_substitution[i]) return after_substitution_S0[i];

      }
    }
    else if (method_number==1)//S1
    {
      for(i=0;i<40;i++)
      {
        if (orignal_char==before_substitution[i]) return after_substitution_S1[i];

      }

    }
    else if(method_number==2)//S2
    {
      for(i=0;i<40;i++)
      {
        if (orignal_char==before_substitution[i]) return after_substitution_S2[i];

      }
    }
    else if(method_number==3)//S3
    {
      for(i=0;i<40;i++)
      {
        if (orignal_char==before_substitution[i]) return after_substitution_S3[i];

      }
    }
    else if(method_number==4)//S4
    {
      for(i=0;i<40;i++)
      {
        if (orignal_char==before_substitution[i]) return after_substitution_S4[i];

      }
    }
  }
  else if (encryption==0)//doing decryption
  {
    if (method_number==0)//S0
    {
      for(i=0;i<40;i++)
      {
        if (orignal_char==after_substitution_S0[i]) return before_substitution[i];

      }
    }
    else if (method_number==1)//S1
    {
      for(i=0;i<40;i++)
      {
        if (orignal_char==after_substitution_S1[i]) return before_substitution[i];

      }

    }
    else if(method_number==2)//S2
    {
      for(i=0;i<40;i++)
      {
      if (orignal_char==after_substitution_S2[i]) return before_substitution[i];

      }
    }
    else if(method_number==3)//S3
    {
      for(i=0;i<40;i++)
      {
        if (orignal_char==after_substitution_S3[i]) return before_substitution[i];

      }
    }
    else if(method_number==4)//S4
    {
      for(i=0;i<40;i++)
      {
        if (orignal_char==after_substitution_S4[i]) return before_substitution[i];

      }
    }
  }
  return '{';//indicate invaild argument values
}

int main()
{
  //Creating socket
  printf("------------------------------------output of the program------------------------\n");
  printf("creating socket\n");
  int newsocket = socket(PF_INET , SOCK_STREAM , 0); // TCP/IP family
  if (newsocket==-1){
    printf("creation failed\n");
    return;

  }
  else{
  printf("socket at server has been created\n");
  }
  //start to binding
  struct sockaddr_in serveraddress;
  serveraddress.sin_family=AF_INET; //IP
  serveraddress.sin_port=htons(7000);// convert host byte order to network byte order
  serveraddress.sin_addr.s_addr=INADDR_ANY; //
  int bind_return=bind(newsocket,(struct sockaddr *)&serveraddress , sizeof(serveraddress));
  if(bind_return<0){
    printf("binding fails\n");
    return;
  }
  else{
    printf("binding complete\n");
  }
  //start listening
  listen(newsocket, 10);
  printf("listening\n");
  struct sockaddr_in client_addr;
  int size=sizeof(struct sockaddr_in);
  int socket_client=accept(newsocket, (struct sockaddr *)&client_addr, &size);
  if(socket_client==-1){
    printf("acception fails\n");
    return;
  }
  else{
    printf("conncetion to %s is accepted\n", inet_ntoa(client_addr.sin_addr));
  }

















 // calculat y_b and send it to client
 printf("getting integer value k.......................\n");
 int n=10; //public number
 int q=12;//public number

 int x_b=4;// private number
 int n_to_power_of_x_b=1;
 int count=0;
 while (count<x_b)
 {
   n_to_power_of_x_b=n_to_power_of_x_b*n;
   count++;
 }
 int y_b[1]={n_to_power_of_x_b%q};
 // send the value of y_b to client
 int return_of_send=send(socket_client, y_b, sizeof(int),0);
 printf("y_b I send is %d", y_b[0]);
 if(return_of_send<0){
   printf("unable to send y_b, send fails\n");
   return;
 }



 printf("trying to recieve the value of y_a from client\n");
 int re_y_a[1];
 int return_of_recieve=recv(socket_client,re_y_a,sizeof(int),0);
 printf("y_a I recieved is %d", re_y_a[0]);
 if(return_of_recieve<0){
   printf("recieve of y_a fails, cannot do the rest\n");
   return;

 }
 else{
   printf("y_a recieved, calculating the k value\n");
 }
 int y_a=re_y_a[0];
 // calculate k value
 int k;
 int y_a_to_the_power_of_x_b=1;
 count=0;
 while(count<x_b)
 {
   y_a_to_the_power_of_x_b=y_a_to_the_power_of_x_b*y_a;
   count++;
 }
 k=y_a_to_the_power_of_x_b % q; //getting k value; should be same as the k value in client side
 //N=5 in our case
 printf("the value of integer k is %d\n",k);
 printf("N=5 in this program, we have 5 security schemes, they are S0 S1 S2 S3 S4, calculating which one to use ......" );
 int method=k % 5; // client and server should both agree with using this scheme
 printf("we will use S%d\n",method);








  // getting data from client;
  char recieve_Str[500];
  int return_recieve=recv(socket_client,recieve_Str, 500, 0);
  if(return_recieve<0)
  {
    printf("cannot recieve from client\n");
    return;

  }
  else{ // successfully reciveing from client
    printf("successfully recived from client\n");
    printf("the recieved message (without decryption)is : %s\n",recieve_Str);
    // start doing decryption:



    //start to do  a transpostion

    int i=0;
    int j=0;
    int o=0;
    int tem=25; //r=5,n=5.
    int number_of_matrix;//calculat how many 5*5 matrix we need to store this message
    if(strlen(recieve_Str)%tem==0)
    {
      number_of_matrix=strlen(recieve_Str)/tem;
    }
    else
    {
      number_of_matrix=strlen(recieve_Str)/tem+1;
    }
    char matrix3[number_of_matrix][5][5];



    //put the recieved message into matrices
    for(i=0;i<number_of_matrix;i++)
    {
      for(j=0;j<5;j++)// indicate which row
      {
        for(o=0;o<5;o++) // indicate which coloum
        {
            matrix3[i][j][o]=recieve_Str[o+5*j+5*5*i];

        }

      }
    }






  // doing transpostion on these matrices
    char matrix4[number_of_matrix][5][5]; // matrices that will store the transposed matrices
    for(i=0;i<number_of_matrix;i++)
    {
      for(j=0;j<5;j++)// indicate which row
      {
        for(o=0;o<5;o++)// indicate which coloum
        {
          matrix4[i][j][o]=matrix3[i][o][j];
        }

      }
    }// done transpostion


    // put transposed matrix into a string
    char actual_recieve_Str[500];
    int index=0;
    for(i=0;i<number_of_matrix;i++)
    {
      for(j=0;j<5;j++)// indicate which row
      {
        for(o=0;o<5;o++)// indicate which coloum
        {
          actual_recieve_Str[index]=matrix4[i][j][o];
          index++;
        }

      }
    }
    actual_recieve_Str[index]='\0';

    // now we have to do a substitution to get orignal message. So we need to call substution function again
    //printf("transposition  completed, doing substitution................\n");
    char  actual_recieve_Str2[500];
    for (i=0;i<strlen(actual_recieve_Str);i++)
    {
      if(actual_recieve_Str[i]=='-') //'-' should only happen at the end
      {
        //actual_reply_Str2[i]='\0';
        break;
      }
      actual_recieve_Str2[i]=substitution(0,method,actual_recieve_Str[i]);//method=0 since we are doing decrpytion
    }
    actual_recieve_Str2[i]='\0';
    printf("succesful decrpytion of recieved message \n" );
    printf("the actual recieved message is :\n");
    printf("%s\n",actual_recieve_Str2);








    //sending reply message to client
    char reply[500]="hello, this is a secure reply message";
    printf("before encrprtion, the reply message need to be sent is :\n");
    printf("%s\n",reply);





    // doing encrprtion

     i=0;
     j=0;
     o=0;

    // doing substution
    for (i=0;i<strlen(reply);i++)
    {
      reply[i]=substitution(1,method,reply[i]);//substitution first. first argument=1 indicate we are doing encrprtion
    }// done substitution



    //start to do a transpostion
    tem=25; //r=5,n=5.
    //calculat how many 5*5 matrix we need
    if(strlen(reply)%tem==0)
    {
      number_of_matrix=strlen(reply)/tem;
    }
    else
    {
      number_of_matrix=strlen(reply)/tem+1;
    }
    char matrix[number_of_matrix][5][5]; // untransposed matrices. matrix[0][x][y] is the frist matrix matrix[1][x][y] is the second matrix and so on.




    //put the substituted message into matrices
    for(i=0;i<number_of_matrix;i++)
    {
      for(j=0;j<5;j++)// indicate which row
      {
        for(o=0;o<5;o++) // indicate which coloum
        {
          if(o+5*j+5*5*i<strlen(reply))
          {
            matrix[i][j][o]=reply[o+5*j+5*5*i];
          }
          else
          {
           matrix[i][j][o]='-';
          }
        }

      }
    }



    // doing transpostion on these matrices
    char matrix2[number_of_matrix][5][5]; // matrices that will store the transposed matrices
    for(i=0;i<number_of_matrix;i++)
    {
      for(j=0;j<5;j++)// indicate which row
      {
        for(o=0;o<5;o++)// indicate which coloum
        {
          matrix2[i][j][o]=matrix[i][o][j];
        }

      }
    }// done transpostion



    // put the value of entries of transposed matrices into a string
    char actual_reply[500];
    index=0;
    for(i=0;i<number_of_matrix;i++)
    {
      for(j=0;j<5;j++)// indicate which row
      {
        for(o=0;o<5;o++)// indicate which coloum
        {
          actual_reply[index]=matrix2[i][j][o];
          index++;
        }

      }
    }
    actual_reply[index]='\0'; // values in the string
    printf("succesfully created encrypted reply message, and it is :\n" );
    printf("%s\n",actual_reply);



    int len=strlen(actual_reply);
    if(send(socket_client,actual_reply,len,0)<0) //send encrypted message
    {
      printf("unable to reply\n");
      return;
    }
    else
    {
      printf("succesfully reply to client\n");

    }
  }
  shutdown(socket_client,2);
  shutdown(newsocket,2);
  close(socket_client);
  close(newsocket);
  printf("------------------------------------end of output------------------------\n");
}
